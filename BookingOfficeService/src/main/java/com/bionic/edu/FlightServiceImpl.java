package com.bionic.edu;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class FlightServiceImpl implements FlightService {

	@Inject
	private FlightDao flightdao;

	@Override
	public Flight findById(int id) {
		return flightdao.findById(id);
	}

	@Override
	public List<Flight> getAllFlights() {
		return flightdao.getAllFlights();
	}

}
