package com.bionic.edu;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class FlightDaoImpl implements FlightDao {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Flight findById(int id) {
		return em.find(Flight.class, id);
	}
	@Override
	public List<Flight> getAllFlights() {
		return em.createQuery("SELECT f FROM Flight f", Flight.class).getResultList();
	}
	
	
}