package com.bionic.edu;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FlightList {
	private List<Flight> listFlights;

	public FlightList() {
		listFlights = new ArrayList<Flight>(); 
	}

	@XmlElementWrapper(name = "listFlights")
	@XmlElement(name = "flight", type = Flight.class)
	public List<Flight> getFlights() {
		return listFlights;
	}

	public void setListFlights(List<Flight> listFlights) {
		this.listFlights = listFlights;
	}
}