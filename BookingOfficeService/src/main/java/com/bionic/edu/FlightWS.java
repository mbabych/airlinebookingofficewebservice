package com.bionic.edu;

import javax.jws.WebService;

import com.bionic.edu.Flight;
@WebService
public interface FlightWS {
	
	String getFlightList();
	
	Flight getFlight(int id);

}
