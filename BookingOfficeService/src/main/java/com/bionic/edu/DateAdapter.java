package com.bionic.edu;

import java.text.SimpleDateFormat;
import java.sql.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class DateAdapter extends XmlAdapter<String, Date> {

	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	@Override
	public String marshal(Date v) throws Exception {
		
		return dateFormat.format(v);
	}

	@Override
	public Date unmarshal(String v) throws Exception {
		 return new java.sql.Date(dateFormat.parse(v).getTime());
	}

}
