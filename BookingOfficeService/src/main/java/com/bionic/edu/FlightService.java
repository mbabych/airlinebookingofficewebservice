package com.bionic.edu;

import java.util.List;

public interface FlightService {

	public Flight findById(int id);
	
	public List<Flight> getAllFlights();
}
