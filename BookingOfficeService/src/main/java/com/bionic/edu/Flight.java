package com.bionic.edu;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlRootElement
@Entity
public class Flight {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int FlightID;
	private String Depar;
	private String Destin;
	private double Fare;
	private java.sql.Date Depar_Date;
	private java.sql.Date Arrival_Date;
	private java.sql.Time Depar_Time;
	private java.sql.Time Arrival_Time;
	private int Capacity;
	
	public Flight(){}

	public int getFlightID() {
		return FlightID;
	}

	public void setFlightID(int flightID) {
		FlightID = flightID;
	}

	public String getDepar() {
		return Depar;
	}

	public void setDepar(String depar) {
		Depar = depar;
	}

	public String getDestin() {
		return Destin;
	}

	public void setDestin(String destin) {
		Destin = destin;
	}

	public double getFare() {
		return Fare;
	}

	public void setFare(double fare) {
		Fare = fare;
	}
	public int getCapacity() {
		return Capacity;
	}

	public void setCapacity(int capacity) {
		Capacity = capacity;
	}

	



	public java.sql.Date getDepar_Date() {
		return Depar_Date;
	}

	public void setDepar_Date(java.sql.Date depar_Date) {
		Depar_Date = depar_Date;
	}

	public java.sql.Date getArrival_Date() {
		return Arrival_Date;
	}

	public void setArrival_Date(java.sql.Date arrival_Date) {
		Arrival_Date = arrival_Date;
	}

	

	public java.sql.Time getDepar_Time() {
		return Depar_Time;
	}

	public void setDepar_Time(java.sql.Time depar_Time) {
		Depar_Time = depar_Time;
	}

	public java.sql.Time getArrival_Time() {
		return Arrival_Time;
	}

	public void setArrival_Time(java.sql.Time arrival_Time) {
		Arrival_Time = arrival_Time;
	}

	@Override
	public String toString() {
		return "Flight [FlightID=" + FlightID + ", Depar=" + Depar + ", Destin=" + Destin + ", Fare=" + Fare
				+ ", Depar_Date=" + Depar_Date + ", Arrival_Date=" + Arrival_Date + ", Depar_Time=" + Depar_Time
				+ ", Arrival_Time=" + Arrival_Time + ", Capacity=" + Capacity + "]";
	}
}

