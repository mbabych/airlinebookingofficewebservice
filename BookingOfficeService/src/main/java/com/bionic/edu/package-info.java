/**
 * 
 */
/**
 * @author nikit_000
 *
 */
@XmlJavaTypeAdapters({
	 @XmlJavaTypeAdapter(type = java.sql.Time.class, value = TimeAdapter.class),
	 @XmlJavaTypeAdapter(type = java.sql.Date.class, value = DateAdapter.class)
	
})
package com.bionic.edu;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
