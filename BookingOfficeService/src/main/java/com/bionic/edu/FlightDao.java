package com.bionic.edu;

import java.util.List;



public interface FlightDao {

	public Flight findById(int id);
	
	public List<Flight> getAllFlights();
}