package com.bionic.edu;

import java.io.StringWriter;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;


@javax.jws.WebService(endpointInterface="com.bionic.edu.FlightWS")
@Named
public class FlightWSBean implements FlightWS {
	private FlightList flights = new FlightList();
	
	public FlightWSBean() {}
	
	@Inject
	FlightService flightService;

	public String getFlightList() {
		try {
			flights.setListFlights(flightService.getAllFlights());
			JAXBContext context = JAXBContext.newInstance(FlightList.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			StringWriter stringWriter = new StringWriter();
			marshaller.marshal(flights, stringWriter);
			return stringWriter.toString();
		} 
		catch (JAXBException e) {
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	public Flight getFlight(int id) {
		return flightService.findById(id);
	}

}
