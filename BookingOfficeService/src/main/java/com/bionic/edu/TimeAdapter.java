package com.bionic.edu;

import java.sql.Time;
import java.text.SimpleDateFormat;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class TimeAdapter extends XmlAdapter<String, Time> {
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Override
	public String marshal(Time v) throws Exception {
		return dateFormat.format(v);
	}

	@Override
	public Time unmarshal(String v) throws Exception {
		return new java.sql.Time(dateFormat.parse(v).getTime());
	}

	

}
